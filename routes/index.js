var express = require('express');
var router = express.Router();


router.get('/views/:page', function(req, res, next) {
   res.render('pages/' + req.params.page);
});

router.get('*', function(req, res, next) {
  res.render('index', { title: 'Judging App' });
});

module.exports = router;
