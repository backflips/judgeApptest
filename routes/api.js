var express = require('express');
var router = express.Router();
var User = require('../models/User');
var jwt = require('jsonwebtoken');
var config = require('../config');

var superSecret = config.secret;

//ROUTES FOR API
//======================================

//Authentication route
router.post('/authenticate', function (req, res) {
  console.log(req.body.username + ' tried to login');
  
  User.findOne({
    username: req.body.username
  }).select('name username password').exec(function (err, user) {
    
    if(err) throw err;
    
    //if no user is found return false
    if(!user) {
      res.json({
        success: false,
        message: 'Authentication failed'
      });
    //if user is found continue
    } else if (user) {
      //check if password is valid
      var validPassword = user.comparePassword(req.body.password);
      if(!validPassword) {
        //if not return false
        res.json({
          success: false,
          message: 'Authentication failed'
        });
      } else {
        //if password is valid create a token and return it
        
        var token = jwt.sign({
          name: user.name,
          username: user.username
        }, superSecret, {
          expiresInMinutes: 1440 //expires in 24 hours
        });
        
        
        //return the token
        res.json({
          success: true,
          message: 'Authentication Successful',
          token: token
        });
      }
    }
  });
});

router.get('/authenticate', function(req, res) {
  res.json({ 
    success: false,
    message: 'Please provide login details'
  });
});


//Middleware
//--------------------------------------

/* GET home page. */
router.get('/', function(req, res, next) {
  res.json({ message: 'Hooray welcome to the api!' });
});

router.use(function(req, res, next) {
  console.log('Someone accessed our api from' + req.ip); 
  
  var token = req.body.token || req.query.token || req.headers['x-access-token'];
  
  //decode the token
  if(token) {
    
    //verify the secret and check the expiry
    jwt.verify(token, superSecret, function(err, decoded) {
      if(err) {
        return res.status(403).send({
          success: false,
          message: 'Failed to Authenticate the token'
        });
      } else {
        //if the token is valid save the token so it can be used elsewhere
        req.decoded = decoded;
        
        next();
      }
    });
    
  } else {
    
    //if there is no token return 403
    return res.status(403).send({
      success: false,
      message: 'login required'
    });
  }
});

//USER ROUTES
//---------------------------------------

router.route('/users')
  //create a user with POST
  .post(function(req, res) {
    //create a new user
    var user = new User();
    
    //save user data
    user.name = req.body.name;
    user.username = req.body.username;
    user.password = req.body.password;
    
    //save the user data and check for errors
    user.save(function(err) {
      if(err) {
        //check for duplicate entry
        if(err.code == 11000)
          return res.json({ success: false, message: 'A user with that username already exists' });
        else 
          return res.send(err);
      }
      res.json({ message: 'User created!' });
    });
  })
  
  //Get all users with GET
  .get(function(req, res) {
    User.find(function(err, users) {
      if(err) res.send(err);
      
      //send the users
      res.json(users);
    });
  })
  
  .delete(function(req,res) {
    User.remove(function(err) {
      if(err) return res.send(err);
      
      res.send({ message: 'users deleted!' });
    });
    
  });

//Specific User routes
router.route('/users/:user_id')
  
  //get a user from user_id with GET
  .get(function(req, res) {
    User.findById(req.params.user_id, function(err, user) {
      if(err) res.send(err);

      res.json(user);
    });
  })
  
  //Update a users info with PUT
  .put(function(req, res) {
    User.findById(req.params.user_id, function(err, user) {
      if(err) res.send(err);
      
      if(req.body.name) user.name = req.body.name;
      if(req.body.username) user.username = req.body.username;
      if(req.body.password) user.password = req.body.password;
      
      user.save(function(err) {
        if(err) res.send(err);
        
        //return a success message
        res.json({ message: 'User updated!' });
      });
    });
  })
  
  //Delete a user with DELETE
  .delete(function(req, res) {
    User.remove({
        _id: req.params.user_id
    }, function(err, user) {
      if(err) return res.send(err);
      
      res.json({ message: 'User deleted!' });
    });
  });
  
router.get('/me', function(req, res) {
    res.send(req.decoded);
});
module.exports = router;
