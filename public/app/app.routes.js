angular.module('app.routes', ['ngRoute'])
	.config(function($routeProvider, $locationProvider) {
		$routeProvider
				
			.when('/', {
				templateUrl		:	'/views/home'
			})
			.when('/login', {
				templateUrl		:	'/views/login',
				controller		:	'mainController',
				controllerAs	:	'login'
			})
			.when('/users', {
				templateUrl		:	'/views/usersall',
				controller		:	'userController',
				controllerAs	:	'user'
			})
			.when('/users/create', {
				templateUrl		:	'/views/userssingle',
				controller		:	'userCreateController',
				controllerAs	:	'user'
			})
			.when('/users/:user_id', {
				templateUrl		:	'/views/userssingle',
				controller		:	'userEditController',
				controllerAs	:	'user'
			})
			.otherwise({
				templateUrl		:	'/views/home'
			});
			
		$locationProvider.html5Mode(true);
	});