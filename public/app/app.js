/// <reference path="../../typings/angularjs/angular.d.ts"/>
angular.module('judgeApp', [
	'ngAnimate',
	'app.routes',
	'authService',
	'mainCtrl',
	'userCtrl',
	'userService'
])

.config(function($httpProvider) {
		$httpProvider.interceptors.push('AuthInterceptor');	
});