angular.module('authService', [])

	//===============================================
	//-----------------Auth Factory------------------
	//	- Handle Login and Logout
	//	- Check if user is logged in
	//	- Get user info
	//===============================================
	.factory('Auth', function($http, $q, AuthToken) {
		var authFactory = {};
		
		//login user
		authFactory.login = function (username, password) {
			return $http.post('/api/authenticate', {
				username	:	username,
				password	:	password
			})
				.success(function(data) {
					AuthToken.setToken(data.token);
					return data;
				});
		};
		
		//logout user
		authFactory.logout = function() {
			AuthToken.setToken();
		};
		
		//check if user is logged in
		authFactory.isLoggedIn = function() {
			if(AuthToken.getToken())
				return true;
			else
				return false;
		};
		
		//get user information
		authFactory.getUser = function() {
			if(AuthToken.getToken())
				return $http.get('/api/me', { cache: true });
			else
				return $q.reject({ message: 'User has no token' });
		};
		
		return authFactory;
	})
	
	//===============================================
	//--------------AuthToken Factory----------------
	//	- get Auth Token
	//	- set the Token
	//	- clear the Token
	//===============================================
	.factory('AuthToken', function($window) {
		var authTokenFactory = {};
		
		//get token from local storage
		authTokenFactory.getToken = function() {
			return $window.localStorage.getItem('token');	
		};
		
		//set or clear a token
		authTokenFactory.setToken = function(token) {
			if(token)
				$window.localStorage.setItem('token', token);
			else
				$window.localStorage.removeItem('token');
		};
		
		return authTokenFactory;
	})
	
	//===============================================
	//-----------Auth Interceptor Factory------------
	//	- Application configuration
	//	- Integrate tokens into requests
	//===============================================
	.factory('AuthInterceptor', function($q, $location, AuthToken) {
		var interceptorFactory = {};
		
		//intercept request
		interceptorFactory.request = function(config) {
			//get the token
			var token = AuthToken.getToken();
			
			//if the token exists add it to the headers
			if(token)
				config.headers['x-access-token'] = token;
				
			return config;
		};
		
		//check for response errors
		interceptorFactory.responseError = function(response) {
			
			//If forbidden, send the user to the login page
			if(response.status == 403)
				$location.path('/login');	
				
			//return the errors
			return $q.reject(response);
		};
		
		return interceptorFactory;
	});