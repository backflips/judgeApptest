var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var bcrypt = require('bcrypt-nodejs');

// user Schema
var UserSchema = new Schema({
	name: String,
	username: { type: String, required: true, index: { unique: true }},
	password: { type: String, required: true, select: false }
});

//hash the password before saving
UserSchema.pre('save', function(next) {
	var user = this;
	
	//Only hash the password if it has been changed or the user is new
	if(!user.isModified('password')) return next();
	
	//generate the hash
	bcrypt.hash(user.password, null, null, function (err, hash) {
		if(err) return next(err);
		
		//set the password to the hash
		user.password = hash;
		next();
	});
});

UserSchema.methods.comparePassword = function(password) {
	var user = this;
	
	return bcrypt.compareSync(password, user.password);	
};

//export the model to mongoose
module.exports = mongoose.model('User', UserSchema);